SimpleXMLElement Object (
  [metadata] => SimpleXMLElement Object (
    [@attributes] => Array (
      [id] => 8726520
      [name] => St Petersburg
      [lat] => 27.7606
      [lon] => -82.6269
    )
  ) 
  [observations] => SimpleXMLElement Object (
    [wl] => Array (
      [0] => SimpleXMLElement Object (
        [@attributes] => Array (
          [t] => 2015-10-13 19:30
          [v] => 2.953
          [s] => 0.010
          [f] => 0,0,0,0
          [q] => p
        )
      )
      [1] => SimpleXMLElement Object (
        [@attributes] => Array (
          [t] => 2015-10-13 19:36
          [v] => 2.946
          [s] => 0.007
          [f] => 0,0,0,0
          [q] => p
          )
        )
        [2] => SimpleXMLElement Object (
        [@attributes] => Array (
          [t] => 2015-10-13 19:42
          [v] => 2.933
          [s] => 0.010
          [f] => 0,0,0,0
          [q] => p
          )
        )
        [3] => SimpleXMLElement Object (
        [@attributes] => Array (
          [t] => 2015-10-13 19:48
          [v] => 2.923
          [s] => 0.010
          [f] => 0,0,0,0
          [q] => p
          )
        )
      )
    )
  )
