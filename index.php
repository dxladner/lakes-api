<?php

$lake_site_id = $_REQUEST['lakesid'];

if($lake_site_id == 'pickwick')
{
  $page = '';
  $result = '';
  $final_result = array();
  $page = file_get_contents('http://www.pickwicklake.info/');
  $page = explode('table', $page);
  $page = explode('Lake Level', $page[4]);
  $page = explode('FEET', $page[1]);
  $result = $page[0];
  ?>
  <strong style="display: inline; font-size: 20px;">
  	Pickwick <?php echo $result; ?><sub>FT</sub> lake level
  </strong>
  <?php
  exit();
}
elseif($lake_site_id == 'cumberland')
{
  $page = '';
  $result = '';
  $final_result = array();
  $page = file_get_contents('http://cumberland.uslakes.info/');
  $page = explode('table', $page);
  $page = explode('Lake Level', $page[4]);
  $page = explode('FEET', $page[1]);
  $result = $page[0];
  ?>
  <strong style="display: inline; font-size: 20px;">
  	Cumberland <?php echo $result; ?><sub>FT</sub> lake level
  </strong>
  <?php
  exit();
}
elseif($lake_site_id == 'kentucky')
{
  $page = '';
  $result = '';
  $final_result = array();
  $page = file_get_contents('http://kentucky.uslakes.info/');
  $page = explode('table', $page);
  $page = explode('Lake Level', $page[4]);
  $page = explode('FEET', $page[1]);
  $result = $page[0];
  ?>
  <strong style="display: inline; font-size: 20px;">
  	Kentucky <?php echo $result; ?><sub>FT</sub> lake level
  </strong>
  <?php
  exit();
}
elseif (($lake_site_id == '8726217') || ($lake_site_id == '8722212') || ($lake_site_id == '8725541') || ($lake_site_id == '8721138'))
{
  $page = '';
  $result = '';
  $final_result = array();
  $page = file_get_contents('http://tidesandcurrents.noaa.gov/noaatidepredictions/NOAATidesFacade.jsp?Stationid='.$lake_site_id);
  $page = explode('table', $page);
  $page = explode('<tr bgcolor="E0E0E0">', $page[5]);
  $page = explode('bgcolor="ffffff">', $page[0]);
  $result = explode(' ', $page[1]);

  ?>
  <strong style="display: inline; font-size: 20px;">
  	<table>
      <tr>
        <th>Date</th>
        <th>Day</th>
        <th>Time</th>
        <th>Hgt</th>
      </tr>
      <tr>
        <td><?php echo str_replace('="25%">', '', $result[137]); ?></td>
        <td><?php echo str_replace('="20%">','', $result[164]); ?></td>
        <td><?php echo $result[239]. $result[240]; ?></td>
        <td><?php echo $result[318]; ?></td>
      </tr>
      <tr>
        <td><?php echo str_replace('="25%">', '', $result[467]); ?></td>
        <td><?php echo str_replace('="20%">','', $result[494]); ?></td>
        <td><?php echo $result[569]. $result[570]; ?></td>
        <td><?php echo $result[648]; ?></td>
      </tr>
      <tr>
        <td><?php echo str_replace('="25%">', '', $result[797]); ?></td>
        <td><?php echo str_replace('="20%">','', $result[824]); ?></td>
        <td><?php echo $result[899]. $result[900]; ?></td>
        <td><?php echo $result[978]; ?></td>
      </tr>
      <tr>
        <td><?php echo str_replace('="25%">', '', $result[1127]); ?></td>
        <td><?php echo str_replace('="20%">','', $result[1154]); ?></td>
        <td><?php echo $result[1229]. $result[1230]; ?></td>
        <td><?php echo $result[1308]; ?></td>
      </tr>
    </table>
  </strong>
  <?php
  exit();
}
elseif ($lake_site_id == '08154500')
{
  $url ="http://waterservices.usgs.gov/nwis/dv/?format=json&sites=".$lake_site_id."&siteType=LK";
}
elseif ($lake_site_id == '373025122065901')
{
  $url ='http://waterservices.usgs.gov/nwis/dv/?format=json&sites=373025122065901&siteType=ES';
}
elseif ($lake_site_id == '02306100')
{
  $url ="http://waterservices.usgs.gov/nwis/dv/?format=json&sites=".$lake_site_id."&siteType=ES";
}
elseif (($lake_site_id == '9414290') || ($lake_site_id == '8726520') || ($lake_site_id == '8443970') || ($lake_site_id == '8772447'))
{
  $url ="http://tidesandcurrents.noaa.gov/api/datagetter?begin_date=20151013 19:30&end_date=20151013 19:50&station=". $lake_site_id."&product=water_level&datum=STND&units=english&time_zone=gmt&application=web_services&format=xml";
  $lib = simplexml_load_file($url);
  $siteId = $lib->metadata->attributes()->id;
  $siteName = $lib->metadata->attributes()->name;
  $level = $lib->observations->wl[0]->attributes()->v;

  echo 'SiteID: ' . $siteId;
  echo '<br>';
  echo 'SiteName: ' . $siteName;
  echo '<br>';
  echo 'Water Level: ' . $level;
  echo '<br>';
  echo 'Datum: STND';
  exit();
}
else
{
  $url = "http://waterservices.usgs.gov/nwis/iv/?format=json&sites=".$lake_site_id."&parameterCd=00065,00062&siteType=LK";
}

$ch = curl_init($url);

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

$data = curl_exec($ch);

//echo "<pre>".var_dump( $data )."</pre>";

$obj = json_decode( $data );

if ($lake_site_id == '08154500')
{

  $siteName = $obj->value->timeSeries[1]->sourceInfo->siteName;
  $siteId = $obj->value->timeSeries[1]->sourceInfo->siteCode[0]->value;
  $lakeLevel = $obj->value->timeSeries[1]->values[0]->value[0]->value;
  $date = $obj->value->timeSeries[1]->values[0]->value[0]->dateTime;
}
elseif ($lake_site_id == '373025122065901')
{
  $siteName = $obj->value->timeSeries[1]->sourceInfo->siteName;
  $siteId = $obj->value->timeSeries[1]->sourceInfo->siteCode[0]->value;
  $lakeLevel = $obj->value->timeSeries[1]->values[0]->value[0]->value;
  $date = $obj->value->timeSeries[1]->values[0]->value[0]->dateTime;
}

else
{
  $siteName = $obj->value->timeSeries[0]->sourceInfo->siteName;
  $siteId = $obj->value->timeSeries[0]->sourceInfo->siteCode[0]->value;
  $lakeLevel = $obj->value->timeSeries[0]->values[0]->value[0]->value;
  $date = $obj->value->timeSeries[0]->values[0]->value[0]->dateTime;
}

?>
<table>
  <tr>
    <td width="100px">
      Lake ID:
    </td>
    <td width="400px">
      Lake Name:
    </td>
    <td width="100px">
      Lake Level:
    </td>
    <td width="300px">
      Date Collected:
    </td>
  </tr>
  <tr>
    <td width="100px">
      <?php echo $siteId; ?>
    </td>
    <td width="400px">
      <?php echo $siteName; ?>
    </td>
    <td width="100px">
      <?php echo $lakeLevel; ?> MSL
    </td>
    <td width="300px">
      <?php echo $date; ?>
    </td>
  </tr>
</table>
